﻿var Login = {
    Login: function () {
        debugger
        $.ajax({
            url: "/Account/Login",
            type: "POST",
            data: $('#form-login').serialize(),
            success: function (data) {
                if (data.Message == 'Success') {
                    window.location.href = "/Faculty/List";
                } else {
                    $('#error-message').text("Your account is not correct!");
                }
            }

        });
    },

    Logout: function () {
        $.ajax({
            url: "/Account/Logout",
            type: "POST",
            success: function (data) {
                window.location.href = data.Message;
            }
        });
    }
}