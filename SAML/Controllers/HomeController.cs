﻿using System.Web.Mvc;
using SAML.Models;
using SAML.SAML_Code;

namespace SAML.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var appSettings = new AppSettings();
            var accountSettings = new AccountSettings();
            AuthRequest authRequest = new AuthRequest();

            var xmlPath = @"C:\Users\trung\source\repos\SAML\SAML\SAML\App_Data\SAMLFile.txt";
            var xmlContent = authRequest.GetXmlContent(xmlPath);
            var defAndBase64EncodeRequest = authRequest.GetDefAndBase64EncodeRequest(xmlContent, AuthRequest.AuthRequestFormat.Base64);
            var samlRequest = Server.UrlEncode(defAndBase64EncodeRequest);
            var relayState = Server.UrlEncode(appSettings.relayState);
            var sigAlg = Server.UrlEncode(appSettings.sigAlg);
            var stringToBeSign = "SAMLRequest=" + samlRequest
                + "&RelayState=" + relayState
                + "&SigAlg=" + sigAlg;
            var signatureBase64Encode = authRequest.Sign(stringToBeSign);
            var signatureUrlEncode = Server.UrlEncode(signatureBase64Encode);
            var samlAuthRequest = "https://lgsp-quangnam.local:9448/samlsso?" + stringToBeSign + "&Signature=" + signatureUrlEncode;
            
            ViewBag.XmlContent = xmlContent;
            ViewBag.DefAndBase64EncodeRequest = defAndBase64EncodeRequest;
            ViewBag.SamlRequest = samlRequest;
            ViewBag.RelayState = relayState;
            ViewBag.SigAlg = sigAlg;
            ViewBag.StringToBeSign = stringToBeSign;
            ViewBag.SignatureBase64Encode = signatureBase64Encode;
            ViewBag.SignatureUrlEncode = signatureUrlEncode;
            ViewBag.SamlAuthRequest = samlAuthRequest;
            
            return View();
        }

        public ActionResult About(string SAMLResponse)
        {
            ViewBag.SAMLResponse = SAMLResponse;

            //AuthResponse authResponse = new AuthResponse();
            //string attribute = authResponse.GetAttributes();
            //ViewBag.Attribute = attribute;

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}