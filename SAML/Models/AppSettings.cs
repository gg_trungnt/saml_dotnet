﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAML.Models
{
    public class AppSettings
    {
        public string assertionConsumerServiceUrl = "http://localhost:9009/Home/About";
        public string issuer = "test-phpsaml";
        public string relayState = "http://vnexpress.net";
        public string sigAlg = "http://www.w3.org/2000/09/xmldsig#rsa-sha1";
    }
}