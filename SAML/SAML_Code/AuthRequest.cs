﻿using System;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;

namespace SAML.SAML_Code
{
    public class AuthRequest
    {
        public enum AuthRequestFormat
        {
            Base64 = 1
        }

        public string GetXmlContent(string xmlPath)
        {
            var xmlContent = System.IO.File.ReadAllText(xmlPath);
            return xmlContent;
        }

        /// <summary>
        /// Returns the byte array of a compress string 
        /// </summary>
        /// <param name="xmlContent">Content of the xml file</param>
        /// <returns></returns>
        public byte[] ToCompressedByteArray(string xmlContent)
        {
            using (MemoryStream inMemStream = new MemoryStream(Encoding.ASCII.GetBytes(xmlContent)),
                                outMemStream = new MemoryStream())
            {
                // create a compression stream with the output stream
                using (var zipStream = new DeflateStream(outMemStream, CompressionMode.Compress, true))
                    // copy the source string into the compression stream
                    inMemStream.WriteTo(zipStream);

                // return the compressed bytes in the output stream
                return outMemStream.ToArray();
            }

            //string middle;
            //var bytes = System.Text.Encoding.UTF8.GetBytes(xmlContent);
            //using (var output = new MemoryStream())
            //{
            //    using (var zip = new DeflaterOutputStream(output, new Deflater(Deflater.BEST_COMPRESSION)))
            //        zip.Write(bytes, 0, bytes.Length);

            //    //middle = Convert.ToBase64String(output.ToArray());
            //    return output.ToArray();
            //}
        }

        /// <summary>
        /// Returns the original string for a compressed base64 encode 
        /// </summary>
        /// <param name="defAndBase64Encode"></param>
        /// <returns></returns>
        public static string ToUncompressedString(string defAndBase64Encode)
        {
            // get the byte array representation for the compressed string
            var compressedBytes = Convert.FromBase64String(defAndBase64Encode);

            // load the byte array into a memory stream
            using (var inMemStream = new MemoryStream(compressedBytes))
            // and decompress the memory stream into the original string
            using (var decompressionStream = new DeflateStream(inMemStream, CompressionMode.Decompress))
            using (var streamReader = new StreamReader(decompressionStream))
                return streamReader.ReadToEnd();
        }

        public string GetDefAndBase64EncodeRequest(string xmlContent, AuthRequestFormat format)
        {
            if (format == AuthRequestFormat.Base64)
            {
                byte[] data = ToCompressedByteArray(xmlContent);

                if (data != null) return Convert.ToBase64String(data);
            }

            return null;
        }

        public string GetXmlSignature(string xmlContent, string sigAlg, string privateKey, string cert)
        {
            // Create a new Xml Document
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xmlContent);
            
            // Create a SignedXml
            SignedXml signedXml = new SignedXml(xmlDocument);
            signedXml.SignedInfo.CanonicalizationMethod = sigAlg; //"http://www.w3.org/2000/09/xmldsig#rsa-sha1";

            //byte[] privateKeyBytes = Convert.FromBase64String(privateKey);
            //var privateKeyObject = Asn1Object.FromByteArray(privateKeyBytes);
            //var privateStruct = RsaPrivateKeyStructure.GetInstance(privateKeyObject);
            ////AsymmetricKeyParameter asymmatricKeyParameter = PrivateKeyFactory.CreateKey(privateKeyBytes);
            ////RsaKeyParameters rsaKeyParameters = (RsaKeyParameters)asymmatricKeyParameter;
            //RSAParameters rsaParameters = new RSAParameters();
            //rsaParameters.Modulus = privateStruct.Modulus.ToByteArrayUnsigned();
            //rsaParameters.Exponent = privateStruct.PublicExponent.ToByteArrayUnsigned();
            //rsaParameters.D = privateStruct.PrivateExponent.ToByteArrayUnsigned();
            //rsaParameters.P = privateStruct.Prime1.ToByteArrayUnsigned();
            //rsaParameters.Q = privateStruct.Prime2.ToByteArrayUnsigned();
            //rsaParameters.DP = privateStruct.Exponent1.ToByteArrayUnsigned();
            //rsaParameters.DQ = privateStruct.Exponent2.ToByteArrayUnsigned();
            //rsaParameters.InverseQ = privateStruct.Coefficient.ToByteArrayUnsigned();
            RSACryptoServiceProvider rsaCryptoServiceProvider = new RSACryptoServiceProvider();
            //rsaCryptoServiceProvider.ImportParameters(rsaParameters);

            // Add the key to the SignedXml document
            signedXml.SigningKey = rsaCryptoServiceProvider;

            // Load certificate 
            X509Certificate2 x509Certificate2 = new X509Certificate2();
            x509Certificate2.Import(Convert.FromBase64String(cert));
            //RSACryptoServiceProvider rsaCryptoServiceProvider = (RSACryptoServiceProvider)x509Certificate2.PrivateKey;
            //signedXml.SigningKey = rsaCryptoServiceProvider;

            KeyInfo keyInfo = new KeyInfo();
            
            KeyInfoX509Data keyInfoX509Data = new KeyInfoX509Data(x509Certificate2);
            keyInfo.AddClause(keyInfoX509Data);
            signedXml.KeyInfo = keyInfo;

            // Create a reference to be signed
            Reference reference = new Reference();
            reference.Uri = "";

            // Create an enveloped and an ExcC14N transformation to be transformed
            XmlDsigEnvelopedSignatureTransform xmlDsigEnv = new XmlDsigEnvelopedSignatureTransform();
            XmlDsigExcC14NTransform xmlDsigExc = new XmlDsigExcC14NTransform();

            // Add an enveloped and an ExcC14N transformation to the reference
            reference.AddTransform(xmlDsigEnv);
            reference.AddTransform(xmlDsigExc);

            // Add the referen to the SignedXml
            signedXml.AddReference(reference);

            // Compute the signature
            signedXml.ComputeSignature();
            
            // Get xml element form SigedXml 
            XmlElement xmlElement = signedXml.GetXml();

            // Append the element to the xml document
            xmlDocument.DocumentElement.AppendChild(xmlDocument.ImportNode(xmlElement, true));

            return xmlDocument.OuterXml;
        }
        
        public string Sign(string strToBeSign, string privateKey, string certificate)
        {
            byte[] data = Encoding.ASCII.GetBytes(strToBeSign);

            X509Certificate2 x509Certificate2 = new X509Certificate2();
            x509Certificate2.Import(Convert.FromBase64String(certificate));
           
            X509Certificate2 requestSigningCert = new X509Certificate2("C:\\wso2carbon.pfx", "wso2carbon");
            RSACryptoServiceProvider rSACryptoServiceProvider = (RSACryptoServiceProvider)requestSigningCert.GetRSAPrivateKey();


            //----------------------------------
            //byte[] keyBuffer = Convert.FromBase64String(privateKey);
            //RSACryptoServiceProvider rsaCryptoServiceProvider = Crypto.DecodeRsaPrivateKey(keyBuffer);
            //x509Certificate2.PrivateKey = rsaCryptoServiceProvider;
            ////----------------------------------

            ////// setup the data to sign
            //System.Security.Cryptography.Pkcs.ContentInfo content = new System.Security.Cryptography.Pkcs.ContentInfo(data);
            //SignedCms signedCms = new SignedCms(content, false);
            //CmsSigner signer = new CmsSigner(SubjectIdentifierType.IssuerAndSerialNumber, x509Certificate2);
            //// create the signature
            //signedCms.ComputeSignature(signer);
            //return Convert.ToBase64String(signedCms.Encode());

            //--------------------//
            //byte[] keyBuffer = Convert.FromBase64String(privateKey);
            //RSACryptoServiceProvider rsaCryptoServiceProvider = Crypto.DecodeRsaPrivateKey(keyBuffer);
            //x509Certificate2.PrivateKey = rsaCryptoServiceProvider;



            //Compute hash using SHA1
            SHA1Managed sha1 = new SHA1Managed();
            byte[] dataHash = sha1.ComputeHash(data);

            System.Security.Cryptography.Pkcs.ContentInfo ci = new System.Security.Cryptography.Pkcs.ContentInfo(dataHash);
            SignedCms cms = new SignedCms(ci);
            CmsSigner signer = new CmsSigner(x509Certificate2);
            signer.IncludeOption = X509IncludeOption.EndCertOnly;

            X509Chain chain = new X509Chain();
            chain.ChainPolicy.RevocationMode = X509RevocationMode.NoCheck;
            chain.Build(x509Certificate2);

            if (chain != null)
            {
                signer.IncludeOption = X509IncludeOption.None;
                X509ChainElementEnumerator enumerator = chain.ChainElements.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    X509ChainElement current = enumerator.Current;
                    signer.Certificates.Add(current.Certificate);
                }
            }

            signer.DigestAlgorithm = new Oid("SHA1");
            signer.Certificate.PrivateKey = rSACryptoServiceProvider;
            cms.ComputeSignature(signer);

            return Convert.ToBase64String(cms.Encode());
        }

        public string Sign(string strToBeSign)
        {
            var path = "C:\\wso2carbon.pfx";
            var password = "wso2carbon";

            var collection = new X509Certificate2Collection();

            collection.Import(path, password, X509KeyStorageFlags.PersistKeySet);

            var certificate = collection[0];

            var privateKey = certificate.PrivateKey as RSACryptoServiceProvider;

            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(strToBeSign);
            writer.Flush();
            stream.Position = 0;

            var signature = privateKey.SignData(stream, "SHA1");

            return Convert.ToBase64String(signature);
        }
    }
}