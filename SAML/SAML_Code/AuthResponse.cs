﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel.Security;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace SAML.SAML_Code
{
    public class AuthResponse
    {
        public string GetAttributes()
        {
            //var doc = LoadXmlDocument(@"C:\Users\trung\source\repos\SAML\SAML\SAML\App_Data\SAMLFile.xml");
            //var cert = new X509Certificate2(@"C:\\wso2carbon.pfx", "wso2carbon");

            //string certificatePath = @"C:\\wso2carbon.pfx";
            //X509Certificate2 cert = new X509Certificate2(certificatePath, "wso2carbon");

            //string samlFilePath = @"C:\Users\trung\source\repos\SAML\SAML\SAML\App_Data\SAMLFile.xml";
            //XmlReader reader = XmlReader.Create(samlFilePath);

            //List<SecurityToken> tokens = new List<SecurityToken>();
            //tokens.Add(new X509SecurityToken(cert));

            //SecurityTokenResolver outOfBandTokenResolver = SecurityTokenResolver.CreateDefaultSecurityTokenResolver(new ReadOnlyCollection<SecurityToken>(tokens), true);
            //SecurityToken securityToken = WSSecurityTokenSerializer.DefaultInstance.ReadToken(reader, outOfBandTokenResolver);

            string certPath = @"C:\\wso2carbon.pfx";
            X509Certificate2 cert = new X509Certificate2();
            cert.Import(certPath, "wso2carbon", X509KeyStorageFlags.PersistKeySet);
            //X509Certificate2 cert = new X509Certificate2(certPath, "LetMeIn!");

            // Open the SAML
            string samlPath = @"C:\Users\trung\source\repos\SAML\SAML\SAML\App_Data\SAMLFile.xml";

            string samlRaw = File.OpenText(samlPath).ReadToEnd();

            XmlReader rdr = XmlReader.Create(samlPath);

            List<System.IdentityModel.Tokens.SecurityToken> tokens = new List<System.IdentityModel.Tokens.SecurityToken>();

            var token = new X509SecurityToken(cert);
            tokens.Add(token);

            SecurityTokenResolver resolver =
                SecurityTokenResolver.CreateDefaultSecurityTokenResolver(
                new System.Collections.ObjectModel.ReadOnlyCollection<SecurityToken>(tokens), true);


            var serializer = new System.ServiceModel.Security.WSSecurityTokenSerializer(System.ServiceModel.Security.SecurityVersion.WSSecurity11);

            //Fails on the next line
            //SecurityToken securityToken = serializer.ReadToken(rdr, resolver);
            SecurityToken securityToken = System.ServiceModel.Security.WSSecurityTokenSerializer.DefaultInstance.ReadToken(rdr, resolver);

            SamlSecurityToken deserializedSaml = securityToken as SamlSecurityToken;

            string result = string.Empty;

            foreach (SamlStatement statement in deserializedSaml.Assertion.Statements)
            {
                SamlAttributeStatement attributeStatement = statement as SamlAttributeStatement;
                if (null != attributeStatement)
                {
                    foreach (SamlAttribute attribute in attributeStatement.Attributes)
                    {
                        result += attribute.AttributeValues.First().ToString() + "\n";
                    }
                }
            }

            return result;
        }

        //public static XmlDocument LoadXmlDocument(string assertionFile)
        //{
        //    using (var fs = File.OpenRead(assertionFile))
        //    {
        //        var document = new XmlDocument { PreserveWhitespace = true };
        //        document.Load(fs);
        //        fs.Close();
        //        return document;
        //    }
        //}
    }
}